//
//  DataStore.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 3/9/23.
//

import Foundation
import SwiftUI

//Clase en la que se guardarán las variables que se gasten en todo el proyecto
class DataStore: ObservableObject {
    
    @Published var losEmpleados: [Empleado] = []
    @Published var losProyectos: [Proyecto] = []
    @Published var losParticipan: [Participan] = []
    
    //Id Empleado Filtrado Dinámico
    @Published var iefd: Int = -1
    //Id Proyecto Filtrado Dinámico
    @Published var ipfd: Int = -1
    
    @Published var modeloiPhone: String = ""
    
    // Constructor
    init() {
        //Inicialmente obtengo el modelo del dispositivo
        if let model = getModel(){
            self.modeloiPhone = model
        }
    }
    
  
    /// Actualiza losEmpleados y losProyectos
    func actualizarData(filtradoDinamico: Bool) {
        
        var funciones: Funciones = Funciones(dataStore: self)
        
        // Cargar empleados
        funciones.anyadirElementos(filtradoDinamico: filtradoDinamico, tabla: "empleados") { empleados, _, _ in
            DispatchQueue.main.async { // Las modificaciones las debe realizar el hilo principal
                if let empleados = empleados {
                    self.losEmpleados = empleados
                } else {
                    print("Error al añadir los empleados")
                }
            }
        }
        
        // Cargar proyectos
        funciones.anyadirElementos(filtradoDinamico: filtradoDinamico, tabla: "proyectos") { _, proyectos, _ in
            DispatchQueue.main.async { // Cambiar al hilo principal
                if let proyectos = proyectos {
                    self.losProyectos = proyectos
                } else {
                    print("Error al añadir los proyectos")
                }
            }
        }
        
        // Cargar registros
        funciones.anyadirElementos(filtradoDinamico: filtradoDinamico, tabla: "participan") { _, _, registros in
            DispatchQueue.main.async { // Cambiar al hilo principal
                if let registros = registros {
                    self.losParticipan = registros
                } else {
                    print("Error al añadir los registros")
                }
            }
        }
    }
    
    
    ///Devuelve un booleano que indica si el empleado o el proyecto que recibe como parámetro está presente en el array de empleados o proyectos
    func isPresent(nuevoRegistro: Registro) -> Bool{
        
        //No es necesario comprobar la clase Participan porque se identifican por el id
        if let nuevoEmpleado = nuevoRegistro as? Empleado {
            
            guard !(losEmpleados.contains(nuevoEmpleado)) else{
                return true
            }
        }else if let nuevoProyecto = nuevoRegistro as? Proyecto {
            
            guard !(losProyectos.contains(nuevoProyecto)) else{
                return true
            }
        }
        //Si todo ha ido bien, el registro no está presente
        return false
    }
    
    
    ///A partir de su id, devuelve el nombre del empleado o del proyecto
    func obtieneNombres(tabla: String, id: Int) -> String{
        
        if tabla == "empleados"{
            for empleado in losEmpleados{
                if empleado.empleadoId == id{
                    return empleado.nombreEmpleado
                }
            }
            return ""
        }else{
            for proyecto in losProyectos{
                if proyecto.proyectoId == id{
                    return proyecto.nombre
                }
            }
            return ""
        }
    }
    
    ///Obtiene el nombre real del modelo del dispositivo
    private func getModel() -> String? {
        var systemInfo = utsname()
        uname(&systemInfo)
        
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPhone1,1": return "iPhone 2G"
        case "iPhone1,2": return "iPhone 3G"
        case "iPhone2,1": return "iPhone 3GS"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3": return "iPhone 4"
        case "iPhone4,1": return "iPhone 4S"
        case "iPhone5,1", "iPhone5,2": return "iPhone 5"
        case "iPhone5,3", "iPhone5,4": return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2": return "iPhone 5s"
        case "iPhone7,2": return "iPhone 6"
        case "iPhone7,1": return "iPhone 6 Plus"
        case "iPhone8,1": return "iPhone 6s"
        case "iPhone8,2": return "iPhone 6s Plus"
        case "iPhone8,4": return "iPhone SE (1st generation)"
        case "iPhone9,1", "iPhone9,3": return "iPhone 7"
        case "iPhone9,2", "iPhone9,4": return "iPhone 7 Plus"
        case "iPhone10,1", "iPhone10,4": return "iPhone 8"
        case "iPhone10,2", "iPhone10,5": return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6": return "iPhone X"
        case "iPhone11,2": return "iPhone XS"
        case "iPhone11,4", "iPhone11,6": return "iPhone XS Max"
        case "iPhone11,8": return "iPhone XR"
        case "iPhone12,1": return "iPhone 11"
        case "iPhone12,3": return "iPhone 11 Pro"
        case "iPhone12,5": return "iPhone 11 Pro Max"
        case "iPhone12,8": return "iPhone SE (2nd generation)"
        case "iPhone13,1": return "iPhone 12 mini"
        case "iPhone13,2": return "iPhone 12"
        case "iPhone13,3": return "iPhone 12 Pro"
        case "iPhone13,4": return "iPhone 12 Pro Max"
        case "iPhone14,2": return "iPhone 13 mini"
        case "iPhone14,3": return "iPhone 13"
        case "iPhone14,4": return "iPhone 13 Pro"
        case "iPhone14,5": return "iPhone 13 Pro Max"
        case "iPhone14,8": return "iPhone SE (3rd generation)"
        case "iPhone15,3": return "iPhone 14 Pro Max"
        default:
            return identifier
        }
    }
}


