//
//  Funciones.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 6/8/23.
//

import Foundation
import SwiftUI


class Funciones{
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    private let PROTOCOLO: String = "http://"
    private let dataStore: DataStore
    
    //Constructor de clase
    init(dataStore: DataStore) {
        self.dataStore = dataStore
    }
    
    
    /// Función para formatear el número con un máximo de 2 decimales. Ejemplo de input válido: 7.5
    func formatearDecimales(_ input: String) -> String {
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        
        if let formattedNumber = numberFormatter.string(from: NSNumber(value: Double(input) ?? 0.0)) {
            return formattedNumber
        }
        return ""
    }
    
    
    ///Sirve para deserializar Registros que vienen en forma de Json. De momento solamente es funcional para múltiples registros
    func deserializerRegistrosJson(data: Data?, tabla: String) -> [Registro]?{
        if let data = data {
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                    let registros = jsonArray.compactMap { self.obtieneRegistro(optionalData: $0, tabla: tabla) }
                    
                    return registros
                } else {
                    print("La respuesta del servidor no es un array JSON.")
                    return nil
                }
            } catch {
                print("Error al procesar la respuesta del servidor: \(error)")
                return nil
            }
        }
        return nil
    }
    
    
    ///Carga y añade inicialmente todos los empleados, proyectos y participan a un array de su clase de manera sincronizada. Utiliza la consulta select all
    func anyadirElementos(filtradoDinamico: Bool, tabla: String, completion: @escaping ([Empleado]?, [Proyecto]?, [Participan]?) -> Void) {
        
        var empleados = [Empleado]()
        var proyectos = [Proyecto]()
        var participans = [Participan]()
        
        let consulta = tabla == "participan" ? "select_with_all" : "select_all"
        
        // Sistema de carga del listado para mostrar todos los empleados y proyectos
        self.extractData(id1: 0, id2: 0, tabla: tabla, consulta: consulta) { data in
            
            // Llamada a la función para deserializar el JSON y obtener registros
            if let registros = self.deserializerRegistrosJson(data: data, tabla: tabla) {
                for registro in registros {
                    if let empleado = registro as? Empleado {
                        empleados.append(empleado)
                    } else if let proyecto = registro as? Proyecto {
                        proyectos.append(proyecto)
                    }else if let participan = registro as? Participan {
                        
                        
                        if !filtradoDinamico{
                            //Añado todos los registros si no se activa el filtrado dinámico
                            participans.append(participan)
                        }else{
                            //Solamente añado los registros que coincidan con el id
                            if !(self.dataStore.iefd == -1), !(self.dataStore.ipfd == -1){
                                if participan.empleadoId == self.dataStore.iefd, participan.proyectoId == self.dataStore.ipfd{
                                    participans.append(participan)
                                }
                            }else if !(self.dataStore.iefd == -1){
                                if participan.empleadoId == self.dataStore.iefd{
                                    participans.append(participan)
                                }
                            } else if !(self.dataStore.ipfd == -1){
                                if participan.proyectoId == self.dataStore.ipfd{
                                    participans.append(participan)
                                }
                            }
                        }
                    }
                }
                completion(empleados, proyectos, participans)
            } else {
                print("No se encontró ningún registro.")
                completion(nil, nil, nil)
            }
        }
    }
    
    
    ///Convierte el coche que le llega como parámetro al formato JSON y lo devuelve en una cadena
    func mostrarJson(jsonData: Data){
        if let jsonString = String(data: jsonData, encoding: .utf8) {
            print("JSON creado:")
            print(jsonString)
        }else{
            print("Error al mostrar el json")
        }
    }
    
    
    ///Convierte el coche que le llega como parámetro al formato JSON y lo devuelve en un Data
    func crearJson(nuevoRegistro: Registro) -> Data{
        do{
            let jsonData = try JSONEncoder().encode(nuevoRegistro)
            return jsonData
        } catch {
            print("Error al codificar el coche como JSON: \(error)")
            return Data()
        }
    }
    
    
    ///Gestiona un registro en la base de datos, tanto un empleado, proyecto o registro (me refiero a la tabla participan). Puede realizar las consultas insert, update y delete
    func gestionarRegistro(crud: String, consulta: String, tabla: String, nuevoRegistro: Registro, completion: @escaping (Result<Void, Error>) -> Void) {
        //TODO: Valores admitidos de crud --> POST, PUT o DELETE
        
        //Siempre que hagas un insert asegúrate de que el nuevoRegistro que le pases tenga el id -1
        //TODO: consulta: "insert", id1: String(nuevoRegistro.id) = "-1", crud: "POST"
        //TODO: consulta: "update", id1: String(nuevoRegistro.id), crud: "PUT"
        //TODO: consulta: "delete", id1: String(nuevoRegistro.id), crud: "DELETE"
        let apiUrl = URL(string: self.generaEndpoint(tabla: tabla, id1: String(nuevoRegistro.id), id2: "-1", consulta: consulta))!
        
        // Configurar la solicitud
        var request = URLRequest(url: apiUrl)
        request.httpMethod = crud
        
        // Codificar el objeto Empleado como JSON, mostrarlo y adjuntarlo a la solicitud
        do {
            let jsonData = crearJson(nuevoRegistro: nuevoRegistro)
            //Visualizar el json creado
            self.mostrarJson(jsonData: jsonData)
            request.httpBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        } catch {
            completion(.failure(error))
            return
        }
        
        // Realizar la solicitud
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            // Verificar la respuesta de la API (puedes ajustar esto según la respuesta de tu API)
            guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                completion(.failure(NSError(domain: "APIError", code: 0, userInfo: nil)))
                return
            }
            
            // La inserción fue exitosa
            completion(.success(()))
        }
        
        task.resume()
    }
    
    
    ///Convierte una cadena a entero y lo devuelve. Si falla devolverá -1
    func conversor(cadena: String) -> Int{
        if let convertedNumber = Int(cadena) {
            return convertedNumber
        } else {
            return -1
        }
    }
    
    
    ///Genera  y devuelve una cadena enpoint para hacer peticiones a la api.
    func generaEndpoint(tabla: String, id1: String, id2: String, consulta: String) -> String{
        //TODO: Cuando solo te llegue un id, gasta siempre el id1, y al 2 asignale "-1"
        
        //Así lo hacía inicialmente, con los intent lo hice con diccionarios
        let contenido = leerArchivo(nombreArchivo: "endpoint").replacingOccurrences(of: "\n", with: "")
        let elementos = splitString(cadena: contenido, delimitador: " ")
        
        let ip = elementos[1]
        let puerto = elementos[3]
        
        switch consulta {
        case "select_with_all":
            return self.PROTOCOLO + ip + ":" + puerto + "/" + tabla + "/?withAll=true"
        case "select_by_id", "update", "delete":
            return self.PROTOCOLO + ip + ":" + puerto + "/" + tabla + "/" + id1
        case "select_all":
            return self.PROTOCOLO + ip + ":" + puerto + "/" + tabla + "/"
        case "select_sum_by_id":
            return self.PROTOCOLO + ip + ":" + puerto + "/" + tabla + "/sum/" + id1
        case "select_sum_horas_empleado_by_proyecto":
            return self.PROTOCOLO + ip + ":" + puerto + "/" + tabla + "/sum/" + id1 + "/" + id2
        case "insert":
            return self.PROTOCOLO + ip + ":" + puerto + "/" + tabla
        default:
            return "Ha ocurrido un error al procesar la consulta"
        }
    }
    
    
    ///Manda la consulta a la API y devuelve el resultado
    func extractData(id1: Int, id2: Int, tabla: String, consulta: String, completion: @escaping (Data?) -> Void) {
        
        let urlChoosed = self.generaEndpoint(tabla: tabla, id1: String(id1), id2: String(id2), consulta: consulta)
        
        guard let url = URL(string: urlChoosed) else {
            print("URL inválida.")
            completion(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("Error: \(error)")
                completion(nil)
                return
            }
            
            completion(data)
        }.resume()
    }
    
    
    ///Si es posible coge los datos que le llegan como parámetro y los devuelve como un optional Double
    func deserializarDoubleJSON(data: Data?) -> Double? {
        if let data = data {
            if let numberString = String(data: data, encoding: .utf8) {
                if let decimalNumber = Double(numberString) {
                    return decimalNumber
                } else {
                    print("No se pudo convertir el número decimal")
                    return nil
                }
            } else {
                print("No se pudo convertir los datos a cadena")
                return nil
            }
        }
        return nil
    }
    
    
    ///Parte una cadena a partir de un delimitador
    func splitString(cadena: String, delimitador: String) -> [String]{
        
        let components = cadena.components(separatedBy: delimitador)
        return components
    }
    
    
    ///Accede a la carpeta del principal del proyecto para leer el archivo endpoint.txt y devuelve su contenido
    func leerArchivo(nombreArchivo: String) -> String{
        if let fileURL = Bundle.main.url(forResource: nombreArchivo, withExtension: "txt") {
            do {
                let fileContent = try String(contentsOf: fileURL, encoding: .utf8)
                return fileContent
            } catch {
                print("Error al leer el archivo:", error)
                return ""
            }
        } else {
            print("El archivo no pudo encontrarse en el proyecto.")
            return ""
        }
    }
    
    
    ///Devuelve un registro a partir del Optional que le llega como parámetro
    func obtieneRegistro(optionalData: [String: Any]?, tabla: String) -> Registro? {
        
        switch tabla {
        case "empleados":
            if let data = optionalData, let id = data["empleadoid"] as? Int, let nombre = data["nombreempleado"] as? String, let telf = data["telefono"] as? String {
                let empleado = Empleado(empleadoId: id, nombreEmpleado: nombre, telefono: telf)
                return empleado
            }
        case "proyectos":
            if let data = optionalData, let id = data["proyectoid"] as? Int, let nombre = data["nombre"] as? String, let descrip = data["descripcion"] as? String {
                let proyecto = Proyecto(proyectoId: id, nombre: nombre, descripcion: descrip)
                return proyecto
            }
        case "participan":
            
            // Manejar los valores individualmente y finalmente construir el objeto Participan
            if let data = optionalData,
               let id = data["registroid"] as? Int,
               let idEmpleado = data["empleadoid"] as? Int,
               let idProyecto = data["proyectoid"] as? Int,
               let horas = data["horas"] as? Double,
               let empleadoData = data["empleado"] as? [String: Any],
               let proyectoData = data["proyecto"] as? [String: Any] {
                
                // Crear objetos Empleado y Proyecto usando sus respectivos valores
                let empleado = Empleado(empleadoId: idEmpleado, nombreEmpleado: empleadoData["nombreempleado"] as? String ?? "", telefono: empleadoData["telefono"] as? String ?? "")
                let proyecto = Proyecto(proyectoId: idProyecto, nombre: proyectoData["nombre"] as? String ?? "", descripcion: proyectoData["descripcion"] as? String ?? "")
                
                return Participan(registroId: id, empleadoId: idEmpleado, proyectoId: idProyecto, horas: horas, empleado: empleado, proyecto: proyecto)
            }
        default:
            print("Tabla inválida")
        }
        
        return nil
    }
    
    
    /// Comprueba la conectividad con la API
    func hayConexion(completion: @escaping (Bool) -> Void) {
        
        let url_prueba_conectividad = self.generaEndpoint(tabla: "empleados", id1: "1", id2: "0", consulta: "select_by_id")
        
        guard let url = URL(string: url_prueba_conectividad) else {
            print("URL inválida")
            completion(false)
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("Error al realizar la solicitud: \(error)")
                completion(false)
            } else if let _ = data {
                // Si se reciben datos, se considera que hay conexión
                completion(true)
            }
        }
        task.resume()
    }
    
    ///Comprueba que las horas introducidas por el usuario sean válidas. Gracias a esta función no se permite que se realicen inserts de más de 10 horas, además, también gestiona el posible error si se introduce 05, Swift lo gestiona y devuelve 5, se insertarían 5 horas cuando realmente queremos insertar 30 minutos.
    func horasValidas(horas: String) -> Bool {
        
        //Si las horas introducidas son más de 10, no se permitirá incurrir esa cantidad de horas
        if let horasFloat = Float(horas) {
            guard horasFloat < 10 else{
                return false
            }
        }
        
        // Obtener el primer caracter
        let primerCaracter = horas[horas.startIndex]
        
        // Verificar si el primer caracter es "0"
        guard primerCaracter == "0" else {
            return true
        }
        
        // Obtener el segundo caracter
        let segundoCaracter = horas[horas.index(after: horas.startIndex)]
        // Verificar si el segundo caracter es ","
        guard segundoCaracter == "," else {
            return false
        }
        
        // Si pasó todas las comprobaciones, la cadena es válida
        return true
    }
    
    ///Obtiene el total de horas para mostrarlas usando el filtrado dinámico
    func obtenerHoras(idEmpleado: Int, idProyecto: Int, tabla: String, completion: @escaping (Double?) -> Void) {
        
        var consulta = ""
        var id1 = -1
        var horas: Double = 0.0
        
        switch tabla {
            
        case "empleados":
            if idProyecto == -2{
                consulta = "select_sum_by_id"
                id1 = idEmpleado
            }
        case "proyectos":
            if idEmpleado == -2{
                consulta = "select_sum_by_id"
                id1 = idProyecto
            }
        case "participan":
            consulta = "select_sum_horas_empleado_by_proyecto"
            id1 = idEmpleado
        default:
            print("Algo ha ocurrido")
        }
        
        self.extractData(id1: id1, id2: idProyecto, tabla: tabla, consulta: consulta) { data in
            if let hours = self.deserializarDoubleJSON(data: data) {
                completion(hours)
            } else {
                completion(0.0)
            }
        }
    }
    
    
    ///Realiza los intent permitidos según lo que se le pase como parámtero,  debe recibir una key del archivo "references.txt"
    func intents(key: String){
        
        let diccionario = obtenerDiccionario(nombreArchivo: "references")
        
        if let value = diccionario[key]{
            let urlString = "\(obtenerUrl(key: key))\(value)"
            
            if let url = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    print("Ha ocurrido un error")
                }
            }
        }
    }
    
    ///Obtiene la url completa para realizar los intent permitidos
    func obtenerUrl(key: String) -> String{
        
        switch key{
        case "developer_mail":
            return "mailto:"
        case "developer_url_app_whatsapp":
            return "https://api.whatsapp.com/send?text="
        case "ubication":
            return "maps://?q="
        case "developer_youtube_video_id":
            return "https://www.youtube.com/watch?v="
        case "developer_instagram_id":
            return "https://www.instagram.com/"
        case "developer_facebook_id":
            return "https://www.facebook.com/"
        case "developer_number":
            return "tel://"
        default:
            return ""
        }
    }
    
    ///Obtiene un diccionario a partir de uno de los archivos de texto de este proyecto
    func obtenerDiccionario(nombreArchivo: String) -> [String: String]{
        
        // Crea un diccionario vacío
        var diccionario = [String: String]()
        let contenido = leerArchivo(nombreArchivo: nombreArchivo).replacingOccurrences(of: "\n", with: "")
        let elementos = splitString(cadena: contenido, delimitador: "%")
        
        for i in 0..<elementos.count / 2 {
            let clave = elementos[i * 2]
            let valor = elementos[i * 2 + 1]
            diccionario[clave] = valor
        }
        return diccionario
    }
}
