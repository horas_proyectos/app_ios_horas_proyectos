//
//  Empleado.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 6/8/23.
//

import Foundation

class Empleado: Registro, Hashable {
    
    var empleadoId: Int?
    var nombreEmpleado: String
    var telefono: String
    
    // Constructor con id de empleado
    init(empleadoId: Int?, nombreEmpleado: String, telefono: String) {
        self.nombreEmpleado = nombreEmpleado
        self.telefono = telefono
        self.empleadoId = empleadoId
        super.init(id: empleadoId ?? -1)
    }
    
    // Constructor sin id de empleado
    init(nombreEmpleado: String, telefono: String) {
        self.nombreEmpleado = nombreEmpleado
        self.telefono = telefono
        self.empleadoId = nil
        super.init(id: -1)
    }
    
    //TODO: Si varios empleados tienen el mismo nombre y teléfono, diremos que son el mismo empleado
    // Implementación de la función hash(into:) requerida por el protocolo Hashable
    func hash(into hasher: inout Hasher) {
        hasher.combine(nombreEmpleado)
        hasher.combine(telefono)
    }

    // Implementación de la función == requerida por el protocolo Equatable
    static func ==(lhs: Empleado, rhs: Empleado) -> Bool {
        return lhs.nombreEmpleado == rhs.nombreEmpleado && lhs.telefono == rhs.telefono
    }
    
    // Implementación requerida de init(from:) para Decodable
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        nombreEmpleado = try container.decode(String.self, forKey: .nombreempleado)
        telefono = try container.decode(String.self, forKey: .telefono)
        
        // Intenta decodificar empleadoId si está presente en el JSON, o déjalo como nil
        empleadoId = try? container.decode(Int.self, forKey: .empleadoid)
        
        // Llama al inicializador de la clase base
        super.init(id: empleadoId ?? -1)
    }
    
    // Implementación personalizada de encode(to:) para utilizar CodingKeys de Empleado
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(nombreEmpleado, forKey: .nombreempleado)
        try container.encode(telefono, forKey: .telefono)
        
        // Codifica empleadoId si está presente, de lo contrario, déjalo fuera del JSON
        if let empleadoId = empleadoId {
            try container.encode(empleadoId, forKey: .empleadoid)
        }
    }
    
    // Enum CodingKeys actualizado para incluir empleadoId como opcional
    enum CodingKeys: String, CodingKey {
        case nombreempleado
        case telefono
        case empleadoid
    }
}


