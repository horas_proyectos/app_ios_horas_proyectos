//
//  Participan.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 27/8/23.
//

import Foundation

class Participan: Registro{
    
    var registroId: Int?
    var empleadoId: Int
    var proyectoId: Int
    var horas: Double
    
    var empleado: Empleado?
    var proyecto: Proyecto?
    
    //Constructor con id sin objetos
    init(registroId: Int?, empleadoId: Int, proyectoId: Int, horas: Double) {
        self.empleadoId = empleadoId
        self.proyectoId = proyectoId
        self.horas = horas
        self.registroId = registroId
        self.empleado = nil
        self.proyecto = nil
        
        super.init(id: self.registroId ?? -1)
    }
    
    //Constructor sin id sin objetos
    init(empleadoId: Int, proyectoId: Int, horas: Double) {

        self.empleadoId = empleadoId
        self.proyectoId = proyectoId
        self.horas = horas
        self.registroId = nil
        self.empleado = nil
        self.proyecto = nil
        
        super.init(id: -1)
    }
    
    //Constructor con id y con objetos
    init(registroId: Int?, empleadoId: Int, proyectoId: Int, horas: Double, empleado: Empleado, proyecto: Proyecto) {
        self.empleadoId = empleadoId
        self.proyectoId = proyectoId
        self.horas = horas
        self.registroId = registroId
        self.empleado = empleado
        self.proyecto = proyecto
        
        super.init(id: self.registroId ?? -1)
    }
    
    //Constructor sin id y con objetos
    init(empleadoId: Int, proyectoId: Int, horas: Double, empleado: Empleado, proyecto: Proyecto) {
        self.empleadoId = empleadoId
        self.proyectoId = proyectoId
        self.horas = horas
        self.registroId = nil
        self.empleado = empleado
        self.proyecto = proyecto
        
        super.init(id: self.registroId ?? -1)
    }
    
    // Implementación requerida de init(from:) para Decodable
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        empleadoId = try container.decode(Int.self, forKey: .empleadoid)
        proyectoId = try container.decode(Int.self, forKey: .proyectoid)
        horas = try container.decode(Double.self, forKey: .horas)
        
        // Intenta decodificar empleadoId si está presente en el JSON, o déjalo como nil
        registroId = try? container.decode(Int.self, forKey: .registroid)
        
        // Intenta decodificar el objeto empleado y proyecto si están presentes en el JSON, o déjalos como nil
        empleado = try? container.decode(Empleado.self, forKey: .empleado)
        proyecto = try? container.decode(Proyecto.self, forKey: .proyecto)
        
        // Llama al inicializador de la clase base
        super.init(id: registroId ?? -1)
    }
    
    // Implementación personalizada de encode(to:) para utilizar CodingKeys de Participan
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(empleadoId, forKey: .empleadoid)
        try container.encode(proyectoId, forKey: .proyectoid)
        try container.encode(horas, forKey: .horas)
        
        // Codifica empleadoId si está presente, de lo contrario, déjalo fuera del JSON
        if let registroId = registroId {
            try container.encode(registroId, forKey: .registroid)
        }
        // Codifica empleado si está presente, de lo contrario, déjalo fuera del JSON
        if let empleado = empleado {
            try container.encode(empleado, forKey: .empleado)
        }
        // Codifica proyecto si está presente, de lo contrario, déjalo fuera del JSON
        if let proyecto = proyecto {
            try container.encode(proyecto, forKey: .proyecto)
        }
    }
    
    // Enum CodingKeys actualizado para incluir empleadoId como opcional
    enum CodingKeys: String, CodingKey {
        case empleadoid
        case proyectoid
        case registroid
        case horas
        case empleado
        case proyecto
    }
}
