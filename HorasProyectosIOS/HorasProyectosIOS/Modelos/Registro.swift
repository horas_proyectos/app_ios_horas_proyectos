//
//  Registro.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 29/8/23.
//

import Foundation

///Representa un registro, esta clase es la clase padre para usar las ventajas que ofrece la herencia y el polimorfismo
class Registro: Identifiable, Codable {
    
    var id: Int
    //Requerido para usar el protocolo Identifiable
    init(id: Int) {
        self.id = id
    }
    
    //Requerido para usar el protocolo Codable
    enum CodingKeys: String, CodingKey {
        case id
    }
}
