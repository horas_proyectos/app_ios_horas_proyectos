//
//  Proyecto.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 27/8/23.
//

import Foundation

class Proyecto: Registro, Hashable {
    
    var proyectoId: Int?
    var nombre: String
    var descripcion: String
    
    //Constructor con id
    init(proyectoId: Int?, nombre: String, descripcion: String) {

        self.nombre = nombre
        self.descripcion = descripcion
        self.proyectoId = proyectoId
        super.init(id: self.proyectoId ?? -1)
    }
    
    //Constructor sin id
    init(nombre: String, descripcion: String) {

        self.nombre = nombre
        self.descripcion = descripcion
        self.proyectoId = nil
        super.init(id: -1)
    }
    
    //TODO: Si dos proyectos tienen el mismo nombre, diremos que son el mismo proyecto
    // Implementación de la función hash(into:) requerida por el protocolo Hashable
    func hash(into hasher: inout Hasher) {
        hasher.combine(nombre)
    }
    // Implementación de la función == requerida por el protocolo Equatable
    static func ==(lhs: Proyecto, rhs: Proyecto) -> Bool {
        return lhs.nombre == rhs.nombre
    }
    
        
    // Implementación requerida de init(from:) para Decodable
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        nombre = try container.decode(String.self, forKey: .nombre)
        descripcion = try container.decode(String.self, forKey: .descripcion)
        
        // Intenta decodificar empleadoId si está presente en el JSON, o déjalo como nil
        proyectoId = try? container.decode(Int.self, forKey: .proyectoid)
        
        // Llama al inicializador de la clase base
        super.init(id: proyectoId ?? -1)
    }
    
    // Implementación personalizada de encode(to:) para utilizar CodingKeys de Empleado
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(nombre, forKey: .nombre)
        try container.encode(descripcion, forKey: .descripcion)
        
        // Codifica empleadoId si está presente, de lo contrario, déjalo fuera del JSON
        if let proyectoId = proyectoId {
            try container.encode(proyectoId, forKey: .proyectoid)
        }
    }
    
    // Enum CodingKeys actualizado para incluir empleadoId como opcional
    enum CodingKeys: String, CodingKey {
        case nombre
        case descripcion
        case proyectoid
    }
}

