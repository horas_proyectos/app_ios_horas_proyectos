//
//  DetalleEmpleadoView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 6/8/23.
//

import Foundation
import SwiftUI
import AVKit

struct DetalleRegistroView: View {
    
    @EnvironmentObject var dataStore: DataStore
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State private var horas: Double = 0.0
    @State public var idProyecto: Int
    @State public var actualizar: Bool = false
    @State public var eliminar: Bool = false
    
    //Aquí se guardarán todos los inputs que pueda poner el usuario al hacer un update, máximo serán 3 de Participan
    @State private var inputsUpdate: [String] = ["", "", ""]
    @State private var tabla: String = ""
    @State private var informacion: String = ""
    @State private var cadena: String = ""
    
    @State private var nuevoRegistro: Registro?
    @State private var nuevoEmpleado: Empleado = Empleado(empleadoId: nil, nombreEmpleado: "", telefono: "")
    @State private var nuevoProyecto: Proyecto = Proyecto(proyectoId: nil, nombre: "", descripcion: "")
    @State private var nuevoParticipan: Participan = Participan(registroId: nil, empleadoId: -2, proyectoId: -2, horas: 0.0)
    
    //Son necesarias en el caso de actualizar un registro (participan)
    @State private var replicaUpdateIdRegistro: [Int] = [-1, -1]
    @State private var replicaUpdateNombresRegistro: [String] = ["", ""]
    @State private var replicaUpdateHoras: Double = 0.0
    
    @State private var colorLetra: Color  = Color.primary
    @State private var colorAviso: Color  = Color.primary
    
    @State private var isShowingConfirmation = false
    @State private var nombreFondo: String = "orange&blue"
    
    
    var registro: Registro
    
    
    var body: some View {
        
        let funciones = Funciones(dataStore: dataStore)
        
        GeometryReader { geometry in
            ZStack{
                //Centrado del texto dinámico
                VStack (alignment: actualizar ? .center : .leading , spacing: 15){
                    
                    if let empleado = registro as? Empleado {
                        
                        HStack{
                            Text("ID:").bold()
                            Text("\(empleado.empleadoId ?? -1)")
                        }
                        .onAppear{
                            nuevoEmpleado.id = empleado.empleadoId ?? -2
                            nuevoEmpleado.empleadoId = empleado.empleadoId
                            nuevoEmpleado.nombreEmpleado = empleado.nombreEmpleado
                            nuevoEmpleado.telefono = empleado.telefono
                            
                            nuevoRegistro = nuevoEmpleado
                            tabla = "empleados"
                            
                            cadena = tabla.replacingOccurrences(of: "s", with: "")
                        }
                        
                        HStack{
                            Text(actualizar ? "Nombre actual:" : "Nombre:").bold()
                            Text("\(empleado.nombreEmpleado)")
                        }
                        HStack{
                            Text(actualizar ? "Teléfono actual:" : "Teléfono:").bold()
                            Text("\(empleado.telefono)")
                        }
                        
                        if actualizar{
                            VStack{
                                TextField("Modifica el nombre aquí", text: $inputsUpdate[0])
                                HStack{
                                    Text("Nombre introducido: ").underline()
                                    Text(inputsUpdate[0])
                                }
                                TextField("Modifica el teléfono aquí", text: $inputsUpdate[1])
                                HStack{
                                    Text("Telefono introducido: ").underline()
                                    Text(inputsUpdate[1])
                                }
                            }.multilineTextAlignment(.center)
                        }
                        
                        HStack{
                            Text("Total horas trabajadas:").bold()
                            
                            if idProyecto == -2 {
                                Text("\(String(format: "%.2f", horas))")
                                    .onAppear {
                                        funciones.obtenerHoras(idEmpleado: empleado.empleadoId ?? -2, idProyecto: idProyecto, tabla: "empleados") { horas in
                                            self.horas = horas ?? 0
                                        }
                                    }
                            }else{
                                Text("\(String(format: "%.2f", horas))")
                                    .onAppear {
                                        funciones.obtenerHoras(idEmpleado: empleado.empleadoId ?? -2, idProyecto: idProyecto, tabla: "participan") { horas in
                                            self.horas = horas ?? 0
                                            print(dataStore)
                                        }
                                    }
                            }
                        }
                    } else if let proyecto = registro as? Proyecto {
                        
                        HStack{
                            Text("ID:").bold()
                            Text("\(proyecto.proyectoId ?? -1)")
                        }.onAppear{
                            nuevoProyecto.id = proyecto.proyectoId ?? -2
                            nuevoProyecto.proyectoId = proyecto.proyectoId
                            nuevoProyecto.nombre = proyecto.nombre
                            nuevoProyecto.descripcion = proyecto.descripcion
                            
                            nuevoRegistro = nuevoProyecto
                            tabla = "proyectos"
                            
                            cadena = tabla.replacingOccurrences(of: "s", with: "")
                        }
                        
                        HStack{
                            Text(actualizar ? "Nombre actual:" : "Nombre:").bold()
                            Text("\(proyecto.nombre)")
                        }
                        
                        //Cambio la forma en que se muestra la descripción según la pantalla
                        if !actualizar{
                            HStack(alignment: .top){
                                Text("Descripción:").bold()
                                Text("\(proyecto.descripcion)")
                            }
                        }else{
                            Text("Descripción actual:").bold()
                            Text("\(proyecto.descripcion)")
                                .multilineTextAlignment(.center)
                        }
                        
                        if actualizar{
                            VStack{
                                TextField("Modifica el nombre aquí", text: $inputsUpdate[0])
                                HStack{
                                    Text("Nombre introducido: ").underline()
                                    Text(inputsUpdate[0])
                                }
                                
                                TextField("Modifica la descripción aquí", text: $inputsUpdate[1])
                                HStack{
                                    Text("Descripción introducida: ").underline()
                                    Text(inputsUpdate[1])
                                }
                                
                            }.multilineTextAlignment(.center)
                        }
                        
                        HStack{
                            Text("Total horas dedicadas:").bold()
                            Text("\(String(format: "%.2f", horas))")
                                .onAppear {
                                    funciones.obtenerHoras(idEmpleado: -2, idProyecto: proyecto.proyectoId ?? -2, tabla: "proyectos") { horas in
                                        self.horas = horas ?? 0
                                    }
                                }
                        }
                    }else if let participan = registro as? Participan {
                        
                        HStack{
                            Text("ID del registro:").bold()
                            Text("\(participan.registroId ?? -2)")
                        }.onAppear{
                            nuevoParticipan.id = participan.proyectoId
                            nuevoParticipan.registroId = participan.registroId
                            nuevoParticipan.proyectoId = participan.proyectoId
                            nuevoParticipan.empleadoId = participan.empleadoId
                            nuevoParticipan.horas = participan.horas
                            
                            replicaUpdateIdRegistro[0] = participan.proyectoId
                            replicaUpdateIdRegistro[1] = participan.empleadoId
                            replicaUpdateHoras = participan.horas
                            
                            nuevoRegistro = nuevoParticipan
                            tabla = "participan"
                            cadena = "registro"
                        }
                        
                        HStack{
                            Text("Proyecto id actual:").bold()
                            Text("\(replicaUpdateIdRegistro[0])")
                        }
                        if let proyecto = participan.proyecto{
                            HStack{
                                Text("Nombre del proyecto actual:").bold()
                                Text("\(replicaUpdateNombresRegistro[0])")
                            }.onAppear{
                                replicaUpdateNombresRegistro[0] = proyecto.nombre
                            }
                        }
                        HStack{
                            Text("Empleado id actual:").bold()
                            Text("\(replicaUpdateIdRegistro[1])")
                        }
                        if let empleado = participan.empleado{
                            HStack{
                                Text("Nombre del empleado actual:").bold()
                                Text("\(replicaUpdateNombresRegistro[1])").onAppear{
                                    replicaUpdateNombresRegistro[1] = empleado.nombreEmpleado
                                }
                            }.onAppear{
                                replicaUpdateNombresRegistro[1] = empleado.nombreEmpleado
                            }
                        }
                        HStack{
                            Text(actualizar ? "Horas actual:" : "Horas:").bold()
                            Text("\(String(format: "%.2f", replicaUpdateHoras))")
                        }
                        
                        if actualizar{
                            VStack{
                                TextField("Modifica el id de proyecto aquí", text: $inputsUpdate[0])
                                HStack{
                                    Text("Proyecto id introducido: ").underline()
                                    Text(inputsUpdate[0])
                                }
                                
                                TextField("Modifica el id de empleado aquí", text: $inputsUpdate[1])
                                HStack{
                                    Text("Empleado id introducido: ").underline()
                                    Text(inputsUpdate[1])
                                }
                                
                                TextField("Modifica las horas trabajadas aquí", text: $inputsUpdate[2])
                                HStack{
                                    Text("Horas introducidas: ").underline()
                                    Text(inputsUpdate[2])
                                }
                            }.multilineTextAlignment(.center)
                        }
                    }else {
                        Text("No se ha proporcionado información del registro")
                            .onAppear{
                                informacion = "No se ha proporcionado información del registro"
                                colorAviso = Color.yellow
                            }
                    }
                    
                    if actualizar{
                        Button(action: {
                            
                            //Comprobaciones antes de realizar el update
                            if inputsUpdate[0].isEmpty, inputsUpdate[1].isEmpty, inputsUpdate[2].isEmpty, tabla == "participan"{
                                informacion = "Aviso! Debe modificar algún campo"
                                colorAviso = Color.yellow
                                
                            }else if inputsUpdate[0].isEmpty, inputsUpdate[1].isEmpty, !(tabla == "participan"){
                                informacion = "¡Ojo! Debe rellenar el nombre o el teléfono para actualizar el empleado"
                                colorAviso = Color.yellow
                                
                            }else{
                                if !inputsUpdate[0].isEmpty{
                                    if !(nuevoEmpleado.empleadoId == nil){
                                        nuevoEmpleado.nombreEmpleado = inputsUpdate[0]
                                        nuevoRegistro = nuevoEmpleado
                                    }else if !(nuevoProyecto.proyectoId == nil){
                                        nuevoProyecto.nombre = inputsUpdate[0]
                                        nuevoRegistro = nuevoProyecto
                                    }else if !(nuevoParticipan.registroId == nil){
                                        
                                        if let convertedNumber = Int(inputsUpdate[0]) {
                                            nuevoParticipan.proyectoId = convertedNumber
                                            nuevoRegistro = nuevoParticipan
                                        } else {
                                            print("No se puede convertir \(inputsUpdate[0]) a entero")
                                            informacion = "Error durante la conversión a número entero"
                                            colorAviso = Color.red
                                        }
                                    }
                                }
                                
                                if !inputsUpdate[1].isEmpty{
                                    if !(nuevoEmpleado.empleadoId == nil){
                                        nuevoEmpleado.telefono = inputsUpdate[1]
                                        nuevoRegistro = nuevoEmpleado
                                    }else if !(nuevoProyecto.proyectoId == nil){
                                        nuevoProyecto.descripcion = inputsUpdate[1]
                                        nuevoRegistro = nuevoProyecto
                                    }else if !(nuevoParticipan.registroId == nil){
                                        
                                        if let convertedNumber = Int(inputsUpdate[1]) {
                                            nuevoParticipan.empleadoId = convertedNumber
                                            nuevoRegistro = nuevoParticipan
                                        } else {
                                            print("No se puede convertir \(inputsUpdate[1]) a entero")
                                        }
                                    }
                                }
                                
                                if !inputsUpdate[2].isEmpty{
                                    if !(nuevoParticipan.registroId == nil){
                                        
                                        if let convertedNumber = Double(inputsUpdate[2]) {
                                            nuevoParticipan.horas = convertedNumber
                                            nuevoRegistro = nuevoParticipan
                                        } else {
                                            print("No se puede convertir \(inputsUpdate[2]) a entero")
                                        }
                                    }
                                }
                                
                                if let registro = nuevoRegistro{
                                    //TODO: UPDATE
                                    // Llamar a la función insertar para realizar el update
                                    funciones.gestionarRegistro(crud: "PUT", consulta: "update", tabla: tabla, nuevoRegistro: registro) { result in
                                        switch result {
                                        case .success:
                                            print("Actualización exitosa")
                                            informacion = "Registro actualizado con éxito"
                                            colorAviso = Color.green
                                            
                                            //Sistema para trampear y actualizar la vista antes de cerrarla
                                            if let participan = registro as? Participan {
                                                
                                                replicaUpdateIdRegistro[0] = participan.proyectoId
                                                replicaUpdateIdRegistro[1] = participan.empleadoId
                                                replicaUpdateHoras = participan.horas
                                                
                                                replicaUpdateNombresRegistro[0] = dataStore.obtieneNombres(tabla: "proyectos", id: participan.proyectoId)
                                                replicaUpdateNombresRegistro[1] = dataStore.obtieneNombres(tabla: "empleados", id: participan.empleadoId)
                                                
                                                cerrar(actualizarData: true)
                                            }else{
                                                //En este caso es necesario actualizar antes de los 3 segundos de espera
                                                dataStore.actualizarData(filtradoDinamico: false)
                                                cerrar(actualizarData: false)
                                            }
                                            
                                        case .failure(let error):
                                            // Manejar el error de Actualización
                                            print("Error al actualizar el registro: \(error)")
                                            informacion = "Error al actualizar el registro"
                                            colorAviso = Color.red
                                            cerrar(actualizarData: false)
                                        }
                                    }
                                }
                            }
                        })
                        {
                            Text("Actualizar")
                                .font(.headline)
                                .frame(width: geometry.size.width / 2, height: 50)
                                .background(Color.orange)
                                .foregroundColor(.white)
                                .cornerRadius(20)
                                .padding(10)
                        }
                        Text(informacion)
                            .foregroundColor(colorAviso)
                            .multilineTextAlignment(.center)
                            .background(.primary)
                            .bold()
                    }
                    
                    if eliminar{
                        Button(action: {
                            
                            isShowingConfirmation = true
                        })
                        {
                            Spacer()
                            Text("Eliminar")
                                .font(.headline)
                                .frame(width: geometry.size.width / 2, height: 50)
                                .background(Color.red)
                                .foregroundColor(.white)
                                .cornerRadius(20)
                                .padding(10)
                            Spacer()
                        }
                        HStack{
                            Spacer()
                            Text(informacion)
                                .multilineTextAlignment(.center)
                                .foregroundColor(colorAviso)
                                .background(Color.black)
                                .bold()
                            Spacer()
                        }
                    }
                    Spacer()
                }
                .padding(.all, 30)
                .frame(maxWidth: .infinity, alignment: .leading)
                
                if let registro = nuevoRegistro{
                    //Muestro la pantalla de confirmación cuando se pulse el botón de eliminar
                    ConfirmationView(isShowingConfirmation: $isShowingConfirmation, tabla: tabla, nuevoRegistro: registro, funciones: funciones)
                        .opacity(isShowingConfirmation ? 1 : 0)
                        .padding(.all, 30)
                        .onDisappear{
                            isShowingConfirmation = false
                        }
                }
            }
            .toolbar {
                ToolbarItem(placement: actualizar || eliminar ? .automatic : .principal) {
                    Text(actualizar ? "Modificar \(cadena)" : eliminar ? "Eliminar \(cadena)" : "Detalle \(cadena)")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        //Ajusto el color de la letra solamente en un caso, por que no se ve bien por el color del fondo escogido
                        .foregroundColor(eliminar && (cadena == "proyecto") ? Color.white : .primary)
                }
            }
        }
        .foregroundColor(eliminar && (cadena == "proyecto") ? Color.white : colorLetra)
        .background(
            Image(nombreFondo)
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
        )
        .onAppear{
            //Cambio de fondo dinámico
            if let empleado = registro as? Empleado {
                if !actualizar, !eliminar{
                    nombreFondo = "orange&blue"
                } else if actualizar{
                    nombreFondo = "orange&black"
                }else{
                    nombreFondo = "orange&red"
                }
            }else if let proyecto = registro as? Proyecto {
                if !actualizar, !eliminar{
                    nombreFondo = "blue"
                } else if actualizar{
                    nombreFondo = "orange&blue"
                }else{
                    nombreFondo = "blue&red"
                }
            }else if let participan = registro as? Participan {
                if actualizar{
                    nombreFondo = "orange&green"
                }else{
                    nombreFondo = "green&red"
                }
            }
        }
    }
    
    
    ///Espera 3 segundos y cierra esta view, puede actualizar los datos guardados justo antes de cerrarla
    func cerrar(actualizarData: Bool){
        //Espera 3 segundos para borrar la información mostrada
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            if actualizarData{
                //En caso de que sea necesario actualizar los datos justo antes de cerrar
                dataStore.actualizarData(filtradoDinamico: false)
            }
            // Cierra la vista actual
            presentationMode.wrappedValue.dismiss()
        }
    }
}


struct Previews_DetalleRegistroView_Previews: PreviewProvider {
    static var previews: some View {
        DetalleRegistroView(idProyecto: 1,actualizar: false, eliminar: false, registro: Registro(id: -1))
    }
}

