//
//  DynamicIslandView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 11/9/23.
//

import Foundation
import SwiftUI


struct DynamicIslandView: View {
    
    @State var title: String?
    @State var expanded = false
    
    var body: some View {
        HStack {
            Image(systemName: "heart.fill")
                .controlSize(.large)
                .foregroundColor(.pink)
            
            Text(title ?? "")
                .foregroundColor(.accentColor)
            
            Image(systemName: "heart.fill")
                .controlSize(.large)
                .foregroundColor(.pink)
        }
        .frame(width: expanded ? UIScreen.main.bounds.width - 22 : 126, height: expanded ? 100 : 37.33)
        .blur(radius: expanded ? 0 : 50)
        .opacity(expanded ? 1 : 0)
        .scaleEffect(expanded ? 1 : 0.5, anchor: .top)
        .background {
            RoundedRectangle(cornerRadius: expanded ? 50 : 63, style: .continuous)
                .fill(.primary)
        }
        .onReceive(NotificationCenter.default.publisher(for: .init("DYNAMIC_ISLAND"))) { object in
            
            if let title = object.object as? String {
                self.title = title
            }
            
            withAnimation(.interactiveSpring(response: 0.6, dampingFraction: 0.6, blendDuration: 0.6)) {
  
                self.expanded = true
                
                //Muestro el Dynamic island durante 3 segundos
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    withAnimation(.interactiveSpring(response: 0.6, dampingFraction: 0.6, blendDuration: 0.6)) {
                        
                        self.expanded = false
                        self.title = nil
                    }
                }
            }
        }
        .offset(y: 11)
        .ignoresSafeArea()
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
    }
}

struct DynamicIslandView_Previews: PreviewProvider {
    static var previews: some View {
        DynamicIslandView(title: "Este es mi texto de prueba")
    }
}
