//
//  InformacionView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 10/9/23.
//

import Foundation
import SwiftUI


///Muestra información relativa a esta app, cómo se usa, por qué se ha creado y para qué
struct InformacionView: View {
    
    var body: some View {
        VStack (alignment: .leading, spacing: 15) {
            
            HStack{
                Text("Proyecto:")
                    .bold()
                Text("HorasProyectos.app")
            }
            
            HStack{
                Text("Desarrollador:")
                    .bold()
                Text("José Sánchez García")
            }
            
            VStack(alignment: .leading){
                Text("Propósito:")
                    .bold()
                Text("Esta app se ha creado con la intención de saber cuánto tiempo he tardado en realizar mis proyectos propios personales, de esta manera, me haré de una idea de cuánto se tarda en realizar un proyecto")
                
                Text("Requisitos:")
                    .bold()
                Text("Para que funcione esta app es necesario que se esté ejecutando el proyecto \"horasAPI\" en mi mac. Si la API se está ejecutando en otra máquina, es necesario tener el mac encendido para acceder a la base de datos")
            }
            
            VStack(alignment: .leading){
                Text("Funcionamiento:")
                    .bold()
                Text("Secciones principales")
                
                Divider()
                HStack(alignment: .top){
                    Text("Buscar:")
                        .foregroundColor(.blue)
                    Text("Se muestran registros de empleados y proyectos. Se puede filtrar de manera dinámica los empleados por proyecto")
                }
                Divider()
                HStack(alignment: .top){
                    Text("Insertar:")
                        .foregroundColor(.green)
                    Text("Permite añadir nuevos empleados y proyectos")
                }
            }
                VStack(alignment: .leading){
                    Divider()
                    HStack(alignment: .top){
                        Text("Fichar:")
                            .foregroundColor(.yellow)
                        Text("Se puede añadir un registro con las horas dedicadas de un trabajador a un proyecto")
                    }
                    Divider()
                    HStack(alignment: .top){
                        Text("Actualizar:")
                            .foregroundColor(.orange)
                        Text("Permite actualizar datos. En la sección de registros permite usar un filtrado dinámico, en el cual se muestran el total de horas dedicadas")
                    }
                    Divider()
                    HStack(alignment: .top){
                        Text("Eliminar:")
                            .foregroundColor(.red)
                        Text("Se pueden eliminar cualquier registro")
                    }
            }
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .multilineTextAlignment(.leading)
        .padding()
        .toolbar {
            ToolbarItem(placement: .principal) {
                Text("Información")
                    .font(.largeTitle)
                    .fontWeight(.bold)
            }
        }
    }
}

struct Previews_InformacionView_Previews: PreviewProvider {
    static var previews: some View {
        InformacionView()
    }
}
