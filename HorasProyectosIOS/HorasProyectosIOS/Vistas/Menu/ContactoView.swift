//
//  ContactoView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 15/9/23.
//

import Foundation
import SwiftUI

///Pantalla que te lleva a contactar con el desarrollador
struct ContactoView: View {
    
    @Environment(\.colorScheme) var colorScheme
    
    @State public var funciones: Funciones
    
    var body: some View {
        //Añado ZStack para que no se active el botón al pulsar fuera de él (en el HStack)
        ZStack{
            VStack{
                HStack(spacing: 20){
                    VStack{
                        Button(action: {
                            funciones.intents(key: "developer_mail")
                        }) {
                            Image(systemName: "envelope.fill")
                                .font(.largeTitle)
                                .frame(maxWidth: 100)
                                .frame(height: 100)
                                .background(colorScheme == .dark ? Color.white : Color.black)
                                .foregroundColor(colorScheme == .dark ? Color.black : Color.white)
                                .cornerRadius(200)
                        }
                        
                        Text("Mail")
                    }
                    
                    VStack{
                        Button(action: {
                            funciones.intents(key: "developer_number")
                        }) {
                            Image(systemName: "phone.fill")
                                .font(.largeTitle)
                                .frame(maxWidth: 100)
                                .frame(height: 100)
                                .background(colorScheme == .dark ? Color.white : Color.black)
                                .foregroundColor(colorScheme == .dark ? Color.black : Color.white)
                                .cornerRadius(200)
                        }
                        Text("Teléfono")
                    }
                    
                    VStack{
                        Button(action: {
                            funciones.intents(key: "ubication")
                        }) {
                            Image(systemName: "location.fill")
                                .font(.largeTitle)
                                .frame(maxWidth: 100)
                                .frame(height: 100)
                                .background(colorScheme == .dark ? Color.white : Color.black)
                                .foregroundColor(colorScheme == .dark ? Color.black : Color.white)
                                .cornerRadius(200)
                        }
                        Text("Ubicación")
                    }
                }
                
                //Iconos personalizados a partir de imágenes
                HStack(spacing: 20){
                    VStack{
                        Button(action: {
                            funciones.intents(key: "developer_facebook_id")
                        }) {
                            Image("FaceIcon")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(maxWidth: 100)
                                .frame(height: 100)
                                .cornerRadius(200)
                        }
                        Text("Facebook")
                    }
                    
                    VStack{
                        Button(action: {
                            funciones.intents(key: "developer_instagram_id")
                        }) {
                            Image("InstaIcon")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(maxWidth: 100)
                                .frame(height: 100)
                                .cornerRadius(200)
                        }
                        Text("Instagram")
                    }
                    
                    VStack{
                        Button(action: {
                            funciones.intents(key: "developer_youtube_video_id")
                        }) {
                            Image("YoutubeIcon")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(maxWidth: 100)
                                .frame(height: 100)
                                .cornerRadius(200)
                        }
                        Text("Youtube")
                    }
                }
                Divider()
                
                Text("Compartir")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundColor(.primary)
                    .padding(.top, 30)
                
                HStack(spacing: 20){
                    VStack{
                        Button(action: {
                            funciones.intents(key: "developer_url_app_whatsapp")
                        }) {
                            Image("WhatsIcon")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(maxWidth: 100)
                                .frame(height: 100)
                                .cornerRadius(200)
                        }
                        Text("WhatsApp")
                    }
                    
                    VStack{
                        Button(action: {
                            funciones.intents(key: "developer_url_app")
                        }) {
                            Image("StoreIcon")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(maxWidth: 100)
                                .frame(height: 100)
                                .cornerRadius(200)
                        }
                        Text("App Store")
                    }
                }
            }
            .font(.headline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Text("Contacto")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                }
            }
        }
    }
}

struct Previews_ContactoView_Previews: PreviewProvider {
    static var previews: some View {
        ContactoView(funciones: Funciones(dataStore: DataStore()))
    }
}
