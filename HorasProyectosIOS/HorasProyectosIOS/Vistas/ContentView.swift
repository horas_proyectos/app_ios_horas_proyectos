//
//  ContentView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 7/7/23.
//

import SwiftUI

///Pantalla inicial de la app
struct ContentView: View {
    
    @EnvironmentObject var dataStore: DataStore
    
    @State private var isConnected: Bool = false
    @State private var isButtonPressed: Bool = false
    
    @State private var contador: Int = 0
    @State private var MAX_INTENTOS: Int = 10
    
    @State private var imposibleConectar: Bool = false
    
    
    var body: some View {

            NavigationView {
                VStack{
                    Text("Horas Proyectos")
                        .foregroundColor(Color.white)
                        .font(.title)
                        .bold()
                    
                    if isConnected{
                        
                        NavigationLink("Acceder", destination: BottomTabBarView(showDynamicIsland: dataStore.modeloiPhone.contains("iPhone 14 Pro")), isActive: $isButtonPressed)
                            .bold()
                            .font(.title2)
                        Spacer()
                    }else if imposibleConectar{
                        Text("Error: No hay conexión con la API").foregroundColor(Color.red)
                        Spacer()
                    }else{
                        Text("Conectando...").foregroundColor(.white)
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: .white))
                            .scaleEffect(1.5)
                        Spacer()
                    }
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .background(
                    Image("AppFondoMain")
                        .resizable()
                        .scaledToFill()
                        .edgesIgnoringSafeArea(.all)
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                )
            }
            .onAppear{
                let funciones = Funciones(dataStore: dataStore)
                conectar(intentos: MAX_INTENTOS, funciones: funciones)
            }
    }
    
    
    ///Realiza intentos de conexión cada 3 segundos, es una función recursiva
    func conectar(intentos: Int, funciones: Funciones) {
        if contador < intentos {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                
                funciones.hayConexion { isConnected in
                    self.isConnected = isConnected

                    contador += 1
                    
                    //En cuanto el usuario acceda, no se realizarán más comprobaciones de conexión
                    guard !self.isButtonPressed else{
                        return
                    }
                    
                    //Cuando se hayan acabado los intentos y no haya conexión, muestro error
                    if contador == intentos, !self.isConnected{
                        self.imposibleConectar = true
                        return
                    }
                    
                    conectar(intentos: intentos, funciones: funciones)
                }
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
