//
//  ListaSeleccionableView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 30/8/23.
//

import Foundation
import SwiftUI

///Pantalla de las listas seleccionables de registros
struct ListSeleccionableView: View {
    
    @EnvironmentObject var dataStore: DataStore
    
    @State private var horas: Double = 0.0
    
    //Acción que se llevará a cabo al seleccionar un elemento (buscar, actualizar, borrar)
    @State public var accion: String
    
    @State public var idProyectoSelected: Int = -2
    @State public var idEmpleadoSelected: Int = -2
    
    //Índice Proyecto Seleccionado
    @State public var ips: String = ""
    //Índice Empleado Seleccionado
    @State public var ies: String = ""
    
    @State private var isDetalleViewActive: Bool = false
    
    var cadena: String
    
    var body: some View {
        
        let funciones = Funciones(dataStore: dataStore)
        
        GeometryReader { geometry in
            VStack {
                if cadena == "empleados"{
                    if isDetalleViewActive && ips != ""{
                        VStack{
                            Text("Horas del empleado dedicadas al proyecto")
                            Text(dataStore.losProyectos[funciones.conversor(cadena: ips)].nombre).bold()
                        }
                    }
                }
                
                    if cadena == "empleados"{
                        VStack{
                            
                            if !isDetalleViewActive, accion == "buscar"  {
                                Text("Selecciona una opción si quieres filtrar las horas del empleado por proyecto:")
                                    .multilineTextAlignment(.center)
                                
                                Menu {
                                    ForEach(dataStore.losProyectos.indices, id: \.self) { i in
                                        Button(dataStore.losProyectos[i].nombre) {
                                            idProyectoSelected = dataStore.losProyectos[i].proyectoId ?? -1
                                            ips = String(i)
                                        }
                                    }
                                } label: {
                                    Text(ips.isEmpty ? "Elige una opción" : dataStore.losProyectos[funciones.conversor(cadena: ips)].nombre)
                                        .foregroundColor(Color.blue)
                                }
                            }
                            
                            
                            List(dataStore.losEmpleados, id: \.id) { empleado in
                                NavigationLink(
                                    destination: DetalleRegistroView(idProyecto: idProyectoSelected == -2 ? -2 : idProyectoSelected, actualizar: accion == "actualizar" ? true : false,
                                                                     eliminar: accion == "eliminar" ? true : false,
                                                                     registro: empleado)
                                    
                                    .onAppear {
                                        isDetalleViewActive = true
                                    }
                                    
                                        .onDisappear {
                                            isDetalleViewActive = false
                                        },
                                    label: {
                                        Text(empleado.nombreEmpleado)
                                            .font(.headline)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .padding(10)
                                    }
                                )
                            }
                        }
                    }else if cadena == "proyectos"{
                        List(dataStore.losProyectos, id: \.id) { proyecto in
                            NavigationLink(
                                destination: DetalleRegistroView(idProyecto: proyecto.proyectoId ?? -2, actualizar: accion == "actualizar" ? true : false, eliminar: accion == "eliminar" ? true : false, registro: proyecto)
                                
                                    .onAppear {
                                        isDetalleViewActive = true
                                    }
                                
                                    .onDisappear {
                                        isDetalleViewActive = false
                                    },
                                label: {
                                    Text(proyecto.nombre)
                                        .font(.headline)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .padding(10)
                                }
                            )
                        }
                    }else if cadena == "registros"{
                        VStack{
                            //Oculto el menú superior si DetalleView se muestra
                            if !isDetalleViewActive, !(accion == "buscar")  {
                                
                                Text("Pulse los botones para activar el filtrado dinámico")
                                    .padding(.top, -20)
                                
                                HStack{
                                    Menu{
                                        ForEach(dataStore.losEmpleados.indices, id: \.self) { i in
                                            Button(dataStore.losEmpleados[i].nombreEmpleado) {
                                                idEmpleadoSelected = dataStore.losEmpleados[i].empleadoId ?? -1
                                                ies = String(i)
                                                
                                                self.filtradoDinamico()
                                                funciones.obtenerHoras(idEmpleado: idEmpleadoSelected, idProyecto: idProyectoSelected, tabla: idProyectoSelected == -2 ? "empleados" : "participan") { horas in
                                                    self.horas = horas ?? 0
                                                }
                                            }
                                        }
                                    }label: {
                                        Text(idEmpleadoSelected == -2 ? "Empleados" : dataStore.losEmpleados[funciones.conversor(cadena: ies)].nombreEmpleado)
                                            .font(.headline)
                                            .frame(width: geometry.size.width / 2.25, height: 50)
                                            .background(Color.orange)
                                            .foregroundColor(.white)
                                            .cornerRadius(20)
                                            .padding(10)
                                    }
                                    
                                    Menu {
                                        ForEach(dataStore.losProyectos.indices, id: \.self) { i in
                                            Button(dataStore.losProyectos[i].nombre) {
                                                idProyectoSelected = dataStore.losProyectos[i].proyectoId ?? -1
                                                ips = String(i)
                                                
                                                self.filtradoDinamico()
                                                funciones.obtenerHoras(idEmpleado: idEmpleadoSelected, idProyecto: idProyectoSelected, tabla: idEmpleadoSelected == -2 ? "proyectos" : "participan") { horas in
                                                    self.horas = horas ?? 0
                                                }
                                            }
                                        }
                                    } label: {
                                        Text(idProyectoSelected == -2 ? "Proyectos" : dataStore.losProyectos[funciones.conversor(cadena: ips)].nombre)
                                            .font(.headline)
                                            .frame(width: geometry.size.width / 2.25, height: 50)
                                            .background(Color.blue)
                                            .foregroundColor(.white)
                                            .cornerRadius(20)
                                            .padding(10)
                                    }
                                }
                            }
                            List(dataStore.losParticipan, id: \.id) { participan in
                                NavigationLink(
                                    destination: DetalleRegistroView(idProyecto: -2, actualizar: accion == "actualizar" ? true : false, eliminar: accion == "eliminar" ? true : false, registro: participan)
                                    
                                        .onAppear {
                                            isDetalleViewActive = true
                                        }
                                    
                                        .onDisappear {
                                            isDetalleViewActive = false
                                        },
                                    label: {
                                        
                                        if let proyecto = participan.proyecto{
                                            if let empleado = participan.empleado{

                                                Text("\(proyecto.nombre) - \(empleado.nombreEmpleado) - \(String(format: "%.2f", participan.horas))h")
                                                    .font(.headline)
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .padding(10)
                                            }
                                        }
                                        
                                        
                                    }
                                )
                            }
                        }.onAppear{
                            limpiar()
                        }
                    }
                
                //Oculto el el total de horas de la lista si se abre el detalle de un registro
                if !isDetalleViewActive{
                    if !(horas == 0.00){
                        Text("Total \(String(format: "%.2f", horas)) horas")
                            .frame(maxWidth: .infinity, alignment: .center)
                            .background(Color.green)
                            .lineLimit(1)
                        Spacer()
                    }
                }
            }
            .navigationBarTitle("Lista", displayMode: .inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    //Text(accion == "buscar" ? "\(cadena.capitalized)" : "\(accion.capitalized) \(cadena)")
                    Text("\(cadena.capitalized)")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .foregroundColor(.primary)
                }
            }
            .padding(.top, 25)
        }
    }
    
    
    ///Limpia todas las estructuras y variables necesarias para un correcto funcionamiento de la View
    func limpiar(){
        //Inicialmente cargo todos los registros de participan y reinicio las variables
        dataStore.actualizarData(filtradoDinamico: false)
        dataStore.iefd = -1
        dataStore.ipfd = -1
        
        horas = 0
        
        ies = ""
        ips = ""
        idProyectoSelected = -2
        idEmpleadoSelected = -2
    }
    
    
    ///Realiza las acciones pertinentes para poder activar el filtrado dinámico y finalmente actualiza los datos aplicando dicho filtrado
    func filtradoDinamico(){
        
        if ies.isEmpty, ips.isEmpty{
            print("No se ha seleccionado ninguna opción")
            //Me quedo con uno o con otro según lo que se pulse
        } else if ies.isEmpty{
            dataStore.ipfd = idProyectoSelected
        } else if ips.isEmpty{
            dataStore.iefd = idEmpleadoSelected
        }else{
            //Si ambos se han pulsado me quedo con los dos
            dataStore.ipfd = idProyectoSelected
            dataStore.iefd = idEmpleadoSelected
        }
        dataStore.actualizarData(filtradoDinamico: true)
    }
}


struct Previews_ListSeleccionableView_Previews: PreviewProvider {
    static var previews: some View {
        ListSeleccionableView(accion: "buscar", cadena: "Elementos")
    }
}
