//
//  ConfirmationView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 7/9/23.
//

import Foundation
import SwiftUI

///Pantalla de confirmación en la que se realiza el delete y se le pregunta al usuario si realmente desea eliminar el registro
struct ConfirmationView: View {
    
    @EnvironmentObject var dataStore: DataStore
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @Binding var isShowingConfirmation: Bool
    
    @State public var tabla: String
    @State public var nuevoRegistro: Registro
    @State public var funciones: Funciones
    
    @State private var informacion: String = ""
    
    @State private var colorAviso: Color  = Color.primary
    @State private var isHidingContent = false
    
    var body: some View {
                
        ZStack {
            Text(informacion)
                .multilineTextAlignment(.center)
                .foregroundColor(colorAviso)
                .background(.black)
                .bold()
                .opacity(isHidingContent ? 1 : 0)
                .offset(y: 60)
            
            VStack(spacing: 10){
                Text("Esta acción no se puede deshacer\n¿Está seguro de que desea continuar?")
                    .font(.headline)
                    .foregroundColor(Color.black)
                    .padding()
                
                HStack {
                    Button(action: {
                        //TODO: DELETE
                        // Llamar a la función delete para eliminar el registro
                        funciones.gestionarRegistro(crud: "DELETE", consulta: "delete", tabla: tabla, nuevoRegistro: nuevoRegistro) { result in
                            switch result {
                            case .success:
                                print("Eliminación exitosa")
                                informacion = "Registro eliminado con éxito\nEspere varios segundos..."
                                colorAviso = Color.green
                                isHidingContent = true
                                
                                //Espera de 3 segundos
                                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                                    dataStore.actualizarData(filtradoDinamico: false)
                                    // Cierra la vista actual
                                    presentationMode.wrappedValue.dismiss()
                                    isShowingConfirmation = false
                                }
                                
                            case .failure(let error):
                                // Manejar el error de eliminar
                                print("Error al eliminar el registro: \(error)")
                                informacion = "Error al eliminar el registro\nPrimero debe eliminar todos los registros de fichaje de horas de este \(tabla.replacingOccurrences(of: "s", with: ""))"
                                colorAviso = Color.red
                                isHidingContent = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                                    // Cierra la vista actual
                                    presentationMode.wrappedValue.dismiss()
                                    isShowingConfirmation = false
                                }
                            }
                        }
                    }) {
                        Text("Sí")
                            .frame(maxWidth: .infinity)
                            .padding()
                            .background(Color.white)
                            .foregroundColor(.blue)
                            .cornerRadius(8)
                            .overlay(
                                RoundedRectangle(cornerRadius: 8)
                                    .stroke(Color.blue, lineWidth: 2)
                            )
                    }
                    
                    Button(action: {

                        isShowingConfirmation = false
                        // Cierra la vista actual
                        presentationMode.wrappedValue.dismiss()
                    }) {
                        Text("No")
                            .frame(maxWidth: .infinity)
                            .padding()
                            .background(Color.white)
                            .foregroundColor(.gray)
                            .cornerRadius(8)
                            .overlay(
                                RoundedRectangle(cornerRadius: 8)
                                    .stroke(Color.gray, lineWidth: 2)
                            )
                    }
                }
            }
            .padding(.all, 15)
            .background(Color.white)
            .cornerRadius(16)
            .opacity(isHidingContent ? 0 : 1)
        }
        .animation(.easeInOut)
        .onAppear{
            informacion = ""
        }
    }
}

