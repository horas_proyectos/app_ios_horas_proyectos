//
//  FicharView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 3/9/23.
//

import Foundation
import SwiftUI

///Tercera View del Bottom Tab Bar, sirve fichar las horas trabajadas de los empleados dedicadas a cada proyecto
struct FicharView: View {
    
    @EnvironmentObject var dataStore: DataStore
    
    //Índice Proyecto Seleccionado
    @State private var ips: String = ""
    //Indice Empleado Seleccionado
    @State private var ies: String = ""
    
    //Id Empleado Seleccionado
    @State private var ides: Int = -2
    //Id Proyecto Seleccionado
    @State private var idps: Int = -2
    
    @State private var empleadoSeleccionado: Empleado? = nil
    @State private var proyectoSeleccionado: Proyecto? = nil
    @State private var nuevoParticipan: Participan? = nil
    
    @State private var inputHoras: String = ""
    @State private var informacion: String = ""
    
    //Gestiona cuando el usuario interactúa con el teclado
    @State private var isEditing = false
    
    @State private var colorAviso: Color  = Color.primary
    
    
    var body: some View {
        
        let funciones = Funciones(dataStore: dataStore)
        
        GeometryReader { geometry in
            VStack{

                //Menu que muestra los empleados seleccionables
                Menu {
                    ForEach(dataStore.losEmpleados.indices, id: \.self) { i in
                        Button(action: {
                            ies = String(i)
                            ides = dataStore.losEmpleados[i].empleadoId ?? -1
                            empleadoSeleccionado = dataStore.losEmpleados[i]
                        }) {
                            Text(dataStore.losEmpleados[i].nombreEmpleado)
                                .font(.largeTitle)
                                .frame(width: 200, height: 60)
                        }
                    }
                } label: {
                    Text(ies.isEmpty ? "Elige un empleado" : dataStore.losEmpleados[funciones.conversor(cadena: ies)].nombreEmpleado)
                        .font(.title)
                        .foregroundColor(Color.orange)
                    
                }
                
                //Menu que muestra los proyectos seleccionables
                Menu {
                    ForEach(dataStore.losProyectos.indices, id: \.self) { i in
                        Button(action: {
                            ips = String(i)
                            idps = dataStore.losProyectos[i].proyectoId ?? -1
                            proyectoSeleccionado = dataStore.losProyectos[i]
                        }) {
                            Text(dataStore.losProyectos[i].nombre)
                                .font(.largeTitle)
                                .frame(width: 200, height: 60)
                        }
                    }
                } label: {
                    Text(ips.isEmpty ? "Elige un proyecto" : dataStore.losProyectos[funciones.conversor(cadena: ips)].nombre)
                        .font(.title)
                        .foregroundColor(Color.blue)
                }
                
                TextField("Introduce aquí las horas trabajadas:", text: $inputHoras, onEditingChanged: { editing in
                    isEditing = editing
                }
                )
                .keyboardType(.decimalPad)
                .multilineTextAlignment(.center)
                .padding(.top, 10)
                .foregroundColor(.primary)
                .onTapGesture {
                    if isEditing {
                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    }
                }
                //Text("Horas introducidas: \(funciones.formatearDecimales(inputHoras.replacingOccurrences(of: ",", with: ".")))")
                Text("Preferiblemente gastar como decimales .25, .5 o .75")
                    .multilineTextAlignment(.center)
                
                Button(action: {
                    
                    if !inputHoras.isEmpty{
                        if !ies.isEmpty{
                            if !ips.isEmpty{
                                
                                var horasValidas = funciones.horasValidas(horas: inputHoras)
                                
                                if horasValidas{
                                    //Es necesario hacer doble replacingOccurrences
                                    let h = funciones.formatearDecimales(inputHoras.replacingOccurrences(of: ",", with: "."))
                                    let horas = h.replacingOccurrences(of: ",", with: ".")

                                    if let horasFloat = Double(horas) {
                                        
                                        if horasFloat >= 0.25{
                                            
                                            //Creación de un participan con empleado y proyecto incluído
                                            if let proyecto = proyectoSeleccionado{
                                                if let empleado = empleadoSeleccionado{
                                                    nuevoParticipan = Participan(empleadoId: ides, proyectoId: idps, horas: horasFloat, empleado: empleado, proyecto: proyecto)
                                                }
                                            }
                                            
                                            if let participan = nuevoParticipan{
                                                // Llamar a la función insertar para realizar la inserción
                                                funciones.gestionarRegistro(crud: "POST", consulta: "insert", tabla: "participan", nuevoRegistro: participan) { result in
                                                    switch result {
                                                    case .success:
                                                        // Inserción exitosa
                                                        print("Inserción exitosa")
                                                        informacion = "Inserción exitosa"
                                                        colorAviso = Color.green
                                                        dataStore.actualizarData(filtradoDinamico: false)
                                                        limpiar()
                                                    case .failure(let error):
                                                        // Manejar el error de inserción
                                                        print("Error al insertar el registro: \(error)")
                                                        informacion = "Error al insertar el registro"
                                                        colorAviso = Color.red
                                                        limpiar()
                                                    }
                                                }
                                            }
                                        } else{
                                            informacion = "Aviso: El valor mínimo para incurrir horas es de 0.25 (15 minutos)"
                                            print(informacion)
                                            colorAviso = Color.yellow
                                            inputHoras = ""
                                        }
                                    } else {
                                        informacion = "Ocurrió un error durante la conversión de tipo"
                                        colorAviso = Color.red
                                        print(informacion)
                                    }
                                }else{
                                    informacion = "Error: Horas insertadas no válidas.\nNo se pueden incurrir más de 10 horas o ha cometido un error y no puso la , de los decimales"
                                    colorAviso = Color.red
                                    print(informacion)
                                }
                            }else{
                                informacion = "Aviso: Debe seleccionar un proyecto"
                                colorAviso = Color.yellow
                                print(informacion)
                            }
                        }else{
                            informacion = "Aviso: Debe seleccionar un empleado"
                            colorAviso = Color.yellow
                            print(informacion)
                        }
                    }else{
                        informacion = "Aviso: Debe introducir las horas trabajadas"
                        colorAviso = Color.yellow
                        print(informacion)
                    }
                    
                    //Espera 3 segundos para borrar la información mostrada
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        informacion = ""
                    }
                }) {
                    Text("Fichar")
                        .font(.headline)
                        .frame(width: geometry.size.width / 2, height: 50)
                        .background(Color.yellow)
                        .foregroundColor(.white)
                        .cornerRadius(20)
                        .padding(10)
                }
                
                Text(informacion)
                    .multilineTextAlignment(.center)
                    .foregroundColor(colorAviso)
                    .background(.primary)
                    .bold()
                
                Image("cronometro")
                    .resizable()
                    .scaledToFill()
                    .offset(y: 70)
            }
            .padding(.horizontal, -40)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding(.all, 40)
            .contentShape(Rectangle())
            .onTapGesture {
                if isEditing {
                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                }
            }
        }
    }
    
    
    ///Reinicia los valores de todas las variables relevantes
    func limpiar(){
        inputHoras = ""
        ies = ""
        ides = -2
        ips = ""
        idps = -2
    }
}


struct Previews_FicharView_Previews: PreviewProvider {
    static var previews: some View {
        FicharView()
    }
}
