//
//  ActualizarView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 4/9/23.
//

import Foundation
import SwiftUI
import AVKit

///Cuarta y quinta View del Bottom Tab Bar la cual te lleva a realizar el update y el delete
struct TablasView: View {
       
    @State private var showingProyectos: Bool = false
    @State private var showingEmpleados: Bool = false
    @State private var showingRegistros: Bool = false
    
    var accion: String
    
    var body: some View {
        
        VStack{
            NavigationView {
                List {
                    
                    NavigationLink(destination: ListSeleccionableView(accion: accion, cadena: "empleados"), isActive: $showingEmpleados) {
                        Label {
                            Text("Empleados")
                        } icon: {
                            Image(systemName: "person")
                                .foregroundColor(accion == "actualizar" ? .orange : .red)
                        }
                    }.foregroundColor(.primary)
                    
                    NavigationLink(destination: ListSeleccionableView(accion: accion, cadena: "proyectos"), isActive: $showingProyectos) {
                        Label {
                            Text("Proyectos")
                        } icon: {
                            Image(systemName: "p.circle")
                                .foregroundColor(accion == "actualizar" ? .orange : .red)
                        }
                    }.foregroundColor(.primary)

                    NavigationLink(destination: ListSeleccionableView(accion: accion, cadena: "registros"), isActive: $showingRegistros) {
                        Label {
                            Text("Registros")
                        } icon: {
                            Image(systemName: "clock")
                                .foregroundColor(accion == "actualizar" ? .orange : .red)
                        }
                    }.foregroundColor(.primary)
                }
                .foregroundColor(.orange)
                .listStyle(SidebarListStyle())
                .navigationBarTitle(!(accion == "actualizar") ? "Eliminar" : "Actualizar", displayMode: .inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        Text("\(accion.capitalized) elementos")
                            .font(.largeTitle)
                            .fontWeight(.bold)
                            .foregroundColor(.primary)
                            .padding(.top, 20)
                    }
                }
            }
            
            if !showingProyectos, !showingEmpleados, !showingRegistros, accion == "actualizar"{
                Text("")
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(
                    Image("elemental")
                        .resizable()
                        .scaledToFill()
                        .edgesIgnoringSafeArea(.all)
                        .offset(y: -1)
                        .offset(x: -10)
                    )
                
            }else if !showingProyectos, !showingEmpleados, !showingRegistros, accion == "eliminar"{
                Text("")
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(
                    Image("deleted")
                        .resizable()
                        .scaledToFill()
                        .edgesIgnoringSafeArea(.all)
                        .offset(y: -1)
                        .offset(x: -100)
                    )
            }
        }
    }
}


struct Previews_TablasView_Previews: PreviewProvider {
    static var previews: some View {
        TablasView(accion: "actualizar")
    }
}
