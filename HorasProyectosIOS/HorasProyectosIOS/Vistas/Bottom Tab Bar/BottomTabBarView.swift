//
//  BottomTabBarView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 27/8/23.
//

import Foundation
import SwiftUI

///View principal que te lleva al resto de Views
struct BottomTabBarView: View {
    
    @EnvironmentObject var dataStore: DataStore
    
    @State public var showDynamicIsland: Bool
    
    @State private var selectedTab: Int = -1
    @State private var fillAnimation: Bool = false
    
    var body: some View {
        ZStack{
            TabView(selection: $selectedTab) {
                SelectView()
                //Esto afectará al color que se muestra en el botón de return
                    .accentColor(self.colores(selectedTab: selectedTab))
                    .tabItem {
                        Label("Buscar", systemImage: "magnifyingglass")
                    }
                    .tag(0)
                
                InsertView()
                    .accentColor(self.colores(selectedTab: selectedTab))
                    .tabItem {
                        Label("Insertar", systemImage: "plus")
                        
                    }
                    .tag(1)
                
                FicharView()
                    .accentColor(self.colores(selectedTab: selectedTab))
                    .tabItem {
                        Label("Fichar", systemImage: "clock")
                    }
                    .tag(2)
                
                TablasView(accion: "actualizar")
                    .accentColor(self.colores(selectedTab: selectedTab))
                    .tabItem {
                        Label("Actualizar", systemImage: "arrow.3.trianglepath")
                    }
                    .tag(3)
                
                TablasView(accion: "eliminar")
                    .zIndex(1)
                    .accentColor(self.colores(selectedTab: selectedTab))
                    .tabItem {
                        Label("Eliminar", systemImage: "trash")
                        
                    }
                    .tag(4)

            }
            .navigationBarBackButtonHidden(true)
            .accentColor(self.colores(selectedTab: selectedTab))
            .onAppear{
                UITabBar.appearance().backgroundColor = .black
                UITabBar.appearance().barTintColor = .white
            }
        }
        .onAppear{
            if showDynamicIsland{
                NotificationCenter.default.post(name: .init("DYNAMIC_ISLAND"), object: "¡Bienvenido!")
            }
        }
        .overlay(alignment: .top) {
            if showDynamicIsland {
                DynamicIslandView()
            }
        }
    }
    
    
    ///Devuelve un color u otro en función de qué botón del Bottom tab bar se pulse
    func colores(selectedTab: Int) -> Color{
        switch selectedTab {
        case 0, -1:
            return Color.blue
        case 1:
            return Color.green
        case 2:
            return Color.yellow
        case 3:
            return Color.orange
        default:
            return Color.red
        }
    }
}

struct Previews_BottomTabBarView_Previews: PreviewProvider {
    static var previews: some View {
        BottomTabBarView(showDynamicIsland: true)
    }
}
