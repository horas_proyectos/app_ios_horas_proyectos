//
//  BuscarView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 28/8/23.
//

import Foundation
import SwiftUI

///Pantalla de Buscar
struct SelectView: View {
    
    @EnvironmentObject var dataStore: DataStore
    
    @State private var showingEmpleados: Bool = false
    @State private var primeraVezMostrada: Bool = true

    @State private var cadena: String = "¿Qué desea buscar?"
    @State private var isMenuVisible = false
    @State private var selectedPage: Int? = nil
    

    
    var body: some View {
        
        let funciones: Funciones = Funciones(dataStore: dataStore)

        NavigationView{
            ZStack{
                VStack{
                    HStack{
                        
                        let tabla = showingEmpleados ? "empleados" : "proyectos"
                        
                        //Botón que muestra el menú lateral oculto
                        Button(action: {
                            withAnimation {
                                isMenuVisible.toggle()
                            }
                        }) {
                            Image(systemName: "line.horizontal.3")
                                .font(.title)
                        }
                        Spacer()
                        
                        //Pantalla del listado de empleados
                        NavigationLink(destination: ListSeleccionableView(accion: "buscar", cadena: tabla), isActive: $showingEmpleados) {
                            Text("Empleados")
                                .foregroundColor(Color.orange)
                                .bold()
                                .padding(.horizontal, 15)
                        }
                        
                        //Pantalla del listado de proyectos
                        NavigationLink(destination: ListSeleccionableView(accion: "buscar", cadena: tabla)) {
                            Text("Proyectos")
                                .bold()
                        }
                    }
                    .padding(.top, 7)
                    .padding(.horizontal, 15)
                    
                    Text(cadena)
                        .padding(.bottom, 650)
                        .font(.title2)
                        .foregroundColor(.white)
                        .bold()
                        .frame(maxWidth: .maximum(400, 400), maxHeight: .maximum(751, 751))
                        .background(
                            Image("AppFondoBuscar")
                                .resizable()
                                .scaledToFill()
                                .edgesIgnoringSafeArea(.all))
                        //Elementos que se muestran en el menú lateral oculto
                        .overlay(
                            List {
                                NavigationLink(destination: InformacionView(), tag: 1, selection: $selectedPage) {
                                    Label("Información", systemImage: "info.circle.fill")
                                }
                                NavigationLink(destination: ContactoView(funciones: funciones)
                                               ,tag: 2
                                               ,selection: $selectedPage) {
                                    Label("Contacto", systemImage: "person.line.dotted.person.fill")
                                }
                            }
                                .listStyle(SidebarListStyle())
                                .frame(width: 220)
                                //Ajusta la posición del menú lateral para que se esconda al pulsar el botón
                                .offset(x: isMenuVisible ? -110 : -440)
                                .opacity(0.75)
                        )
                }
            }
            .navigationBarTitle("Buscar", displayMode: .inline)
            .onAppear {
                //Inicialmente obtengo todos los empleados y proyectos
                dataStore.actualizarData(filtradoDinamico: false)
                }
        }
    }
}
    

        




struct Previews_SelectView_Previews: PreviewProvider {
    
    static var previews: some View {
        SelectView()
    }
}
