//
//  InsertView.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 1/9/23.
//

import Foundation
import SwiftUI
import ContactsUI

///Segunda View del Bottom Tab Bar en la que se pueden realizar inserts a la base de datos
struct InsertView: View {
    
    @State private var isInsertViewActive: Bool = false
    @State private var optionSelected: StringItem? = nil
    
    // Gracias a esta estructura haremos que optionSelected siempre tenga un valor
    struct StringItem: Identifiable {
        let id = UUID()
        let value: String
    }
    
    let elementos = [
        "empleados",
        "proyectos"
    ]
    
    var body: some View {
        NavigationView {
            VStack {
                ForEach(elementos, id: \.self) { elemento in
                    Button(action: {
                        // Acción a realizar cuando se presiona el botón
                        // Por ejemplo, navegación a la siguiente pantalla
                        isInsertViewActive = true
                        optionSelected = StringItem(value: elemento)
                    }) {
                        Text(elemento)
                            .font(.largeTitle)
                            .frame(maxWidth: .infinity)
                            .frame(height: 300)
                            .background(elemento == "empleados" ? Color.orange : Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(20)
                            .padding(10)
                            .opacity(0.5)
                    }
                    .toolbar {
                        ToolbarItem(placement: .principal) {
                            Text("Seleccione una opción")
                                .font(.largeTitle)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                                .padding(.top, 60)
                        }
                    }
                    .sheet(item: $optionSelected) { seleccion in
                        // Contenido de la pantalla siguiente
                        MySheetContent(optionSelected: seleccion.value)
                    }
                }
            }
            .background(
                Image("AppFondoInsert")
                    .resizable()
                    .scaledToFill()
                    .edgesIgnoringSafeArea(.bottom)
                    .padding(.top, 30)
            )
        }
    }
}



///Pantalla de inserción de empleados y proyectos
struct MySheetContent: View{
    
    @EnvironmentObject var dataStore: DataStore
    
    @State private var nuevoNombreEmpleado: String = ""
    @State private var nuevoTelefonoEmpleado: String = ""
    @State private var nuevoNombreProyecto: String = ""
    @State private var nuevaDescripcionProyecto: String = ""
    
    @State private var informacion: String = ""
    @State private var nombreFondo: String = "azul"
    
    @State private var colorAviso: Color  = Color.primary
    
    @State private var selectedContact: CNContact?
    @State private var isContactPickerPresented = false
    
    let optionSelected: String
    
    
    var body: some View {
        
        let funciones = Funciones(dataStore: dataStore)
        
        GeometryReader { geometry in
            VStack {
                Text("Insertar \(optionSelected)")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.bottom, 150)
                    .padding(.top, 125)
                
                if optionSelected == "empleados"{
                    TextField("Nombre del nuevo empleado:", text: $nuevoNombreEmpleado)
                        .multilineTextAlignment(.center)
                    TextField("Teléfono del nuevo empleado:", text: $nuevoTelefonoEmpleado)
                        .multilineTextAlignment(.center)
                }else{
                    TextField("Nombre del nuevo proyecto:", text: $nuevoNombreProyecto)
                        .multilineTextAlignment(.center)
                    TextField("Descripción del nuevo proyecto:", text: $nuevaDescripcionProyecto)
                        .multilineTextAlignment(.center)
                }
                
                Button(action: {
                    
                    if optionSelected == "empleados"{
                        
                        if !nuevoNombreEmpleado.isEmpty{
                            if !nuevoTelefonoEmpleado.isEmpty{
                                
                                //Creación del nuevo empleado
                                let nuevoEmpleado = Empleado(nombreEmpleado: nuevoNombreEmpleado.trimmingCharacters(in: .whitespacesAndNewlines), telefono: nuevoTelefonoEmpleado.trimmingCharacters(in: .whitespacesAndNewlines))
                                
                                var empleadoDuplicado = dataStore.isPresent(nuevoRegistro: nuevoEmpleado)
                                
                                if !empleadoDuplicado{
                                    // Llamar a la función insertar para realizar la inserción
                                    funciones.gestionarRegistro(crud: "POST", consulta: "insert", tabla: optionSelected, nuevoRegistro: nuevoEmpleado) { result in
                                        switch result {
                                        case .success:
                                            // Inserción exitosa
                                            print("Inserción exitosa")
                                            informacion = "Inserción existosa"
                                            dataStore.actualizarData(filtradoDinamico: false)
                                            colorAviso = Color.green
                                            limpiarTextField()
                                        case .failure(let error):
                                            // Manejar el error de inserción
                                            print("Error al insertar empleado: \(error)")
                                            informacion = "Error al insertar empleado"
                                            colorAviso = Color.red
                                            limpiarTextField()
                                        }
                                    }
                                }else{
                                    informacion = "Empleado repetido"
                                    colorAviso = Color.red
                                }
                            }else{
                                informacion = "¡Ojo! Debe rellenar el campo del teléfono"
                                colorAviso = Color.yellow
                            }
                        }else{
                            informacion = "Aviso. Debe rellenar el campo del nombre"
                            colorAviso = Color.yellow
                        }
                    }else{
                        
                        if !nuevoNombreProyecto.isEmpty{
                            if !nuevaDescripcionProyecto.isEmpty{
                                
                                //Creación del nuevo proyecto
                                let nuevoProyecto = Proyecto(
                                    nombre: nuevoNombreProyecto.trimmingCharacters(in: .whitespacesAndNewlines),
                                    descripcion: nuevaDescripcionProyecto.trimmingCharacters(in: .whitespacesAndNewlines))
                                
                                var proyectoDuplicado = dataStore.isPresent(nuevoRegistro: nuevoProyecto)
                                
                                if !proyectoDuplicado{
                                    //TODO: INSERT
                                    // Llamar a la función insertar para realizar la inserción
                                    funciones.gestionarRegistro(crud: "POST", consulta: "insert", tabla: optionSelected, nuevoRegistro: nuevoProyecto) { result in
                                        switch result {
                                            
                                        case .success:
                                            print("Inserción exitosa")
                                            informacion = "Inserción existosa"
                                            colorAviso = Color.green
                                            dataStore.actualizarData(filtradoDinamico: false)
                                            limpiarTextField()
                                        case .failure(let error):
                                            print("Error al insertar proyecto: \(error)")
                                            informacion = "Error al insertar el proyecto"
                                            colorAviso = Color.red
                                            limpiarTextField()
                                        }
                                    }
                                }else{
                                    informacion = "Proyecto repetido"
                                    colorAviso = Color.red
                                }
                            }else{
                                informacion = "¡Ojo! Debe rellenar el campo de la descripción"
                                colorAviso = Color.yellow
                            }
                        }else{
                            informacion = "Aviso. Debe rellenar el campo del nombre"
                            colorAviso = Color.yellow
                        }
                    }
                    
                    //Espera 3 segundos para borrar la información mostrada
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        informacion = ""
                    }
                }) {
                    Text("Insertar")
                        .font(.headline)
                        .frame(width: geometry.size.width / 2, height: 50)
                        .background(Color.green)
                        .foregroundColor(.white)
                        .cornerRadius(20)
                        .padding(10)
                }
                Text(informacion)
                    .multilineTextAlignment(.center)
                    .foregroundColor(colorAviso)
                    .background(Color.white)
                    .bold()
                
                //Sección para seleccionar un contacto como empleado
                if optionSelected == "empleados"{
                    
                    Button("Selecionar de mis contactos"){
                        isContactPickerPresented.toggle()
                    }
                    .sheet(isPresented: $isContactPickerPresented) {
                        ContactPickerView(selectedContact: $selectedContact)
                            .onDisappear{
                                //Muestro el contacto seleccionado en los TextField para realizar la inserción
                                if let contact = selectedContact {
                                    nuevoNombreEmpleado = "\(contact.givenName) \(contact.familyName)"
                                    nuevoTelefonoEmpleado = "\(contact.phoneNumbers.first?.value.stringValue ?? "Sin número")"
                                }
                            }
                    }
                }
                
            }
            .onAppear{
                //Cambio dinámico del fondo mostrado
                if optionSelected == "empleados"{
                    nombreFondo = "naranja"
                }else{
                    nombreFondo = "azul"
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(
                Image(nombreFondo)
                    .resizable()
                    .scaledToFill()
                    .edgesIgnoringSafeArea(.all)
            )
        }
    }
    
    //constructor
    init(optionSelected: String) {
        self.optionSelected = optionSelected
    }
    
    
    ///Reinicia el valor de los TextField después de cada intento de inserción
    func limpiarTextField(){
        nuevoNombreEmpleado = ""
        nuevoTelefonoEmpleado = ""
        nuevoNombreProyecto = ""
        nuevaDescripcionProyecto = ""
    }
}



///Pantalla de selección de contactos
struct ContactPickerView: UIViewControllerRepresentable {
    @Binding var selectedContact: CNContact?
    
    func makeUIViewController(context: Context) -> CNContactPickerViewController {
        let picker = CNContactPickerViewController()
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: CNContactPickerViewController, context: Context) {
        // No es necesario hacer nada aquí
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }
    
    class Coordinator: NSObject, CNContactPickerDelegate {
        let parent: ContactPickerView
        
        init(_ parent: ContactPickerView) {
            self.parent = parent
        }
        
        func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
            parent.selectedContact = contact
        }
    }
}


struct Previews_InsertView_Previews: PreviewProvider {
    static var previews: some View {
        InsertView()
    }
}
