//
//  HorasProyectosIOSApp.swift
//  HorasProyectosIOS
//
//  Created by Jose Sanchez Garcia on 7/7/23.
//

import SwiftUI

//TODO: Posibles mejoras:
//-Podrías añadir una foto de cada empleado, guardada en la base de datos codificada y obtenerla desde esta app, es por aprendizaje

//TODO: CRUD
//Select: DataStore.swift
//Insert: InsertView.swift
//Update: DetalleRegistroView.swift
//Delete: ConfirmationView.swift

//La pantalla de Actualizar del bottom tab bar es TablasView (Te lleva al update y al delete)

//PUT, UPDATE y DELETE --> Funciones.gestionarRegistro()
//SELECT ALL --> Funciones.extractData(), Funciones.anyadirElementos()
//SELECT BY ID--> ContentView.conectar()/Funciones.hayConexion
//SUM --> Funciones.obtenerHoras()

@main
struct HorasProyectosIOSApp: App {
    
    @StateObject private var dataStore = DataStore()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(dataStore)
        }
    }
    
    init() {
        // Personalizar la apariencia de la barra de pestañas por defecto
        let tabBarAppearance = UITabBarAppearance()
        tabBarAppearance.backgroundColor = UIColor.black
        UITabBar.appearance().standardAppearance = tabBarAppearance
    }
}

